#!/usr/bin/env python3

import argparse
from collections import defaultdict
from enum import Enum
import os
import re
import sys

from lib import out_file, write_csv_dict
from lib.telnet import telnet_exec


class Direction(Enum):
    NONE = -1
    DOWN = 0
    UP = 1


class VDSLMode(Enum):
    M_UNKNOWN = 'Unknown'
    M_17a = '17a'
    M_35b = '35b'


class Section(Enum):
    NONE = 'none'
    HLOG = 'hlog'
    QLN = 'qln'
    SNR = 'snr'


_steps = {
    VDSLMode.M_17a: 8,
    VDSLMode.M_35b: 16,
}


def parse_status(lines):
    data = defaultdict(lambda: defaultdict(lambda: [None, None]))
    mode = VDSLMode.M_UNKNOWN

    section = Section.NONE
    direction = Direction.NONE

    sizes = None

    for _line in lines:

        line = _line.strip()

        # metadata
        if line.startswith('Running Mode'):
            # print('Running Mode', file=sys.stderr)
            section = Section.NONE
            direction = Direction.NONE
            sizes = None
            tokens = line.split(':')
            if len(tokens) > 1:
                m = tokens[1].split()
                if len(m) > 0:
                    if m[0] == '17A':
                        mode = VDSLMode.M_17a
                    elif m[0] == '35B':
                        mode = VDSLMode.M_35b
                print('VDSL mode: %s' % mode.value, file=sys.stderr)

        # look for a section header
        elif 'Delt HLOG' in line:
            print('found section: HLOG', file=sys.stderr)
            section = Section.HLOG
            direction = Direction.NONE
            sizes = None

        elif 'Delt QLN' in line:
            print('found section: QLN', file=sys.stderr)
            section = Section.QLN
            direction = Direction.NONE
            sizes = None

        elif 'Delt SNR' in line:
            print('found section: SNR', file=sys.stderr)
            section = Section.SNR
            direction = Direction.NONE
            sizes = None

        elif section == Section.NONE:
            continue

        # look for a group size header
        if line.startswith('GroupSize'):
            direction = Direction.NONE
            tokens = line.split()
            if len(tokens) == 4:
                sizes = list(map(int, tokens[2:4]))
                # print('sizes=%r' % sizes, file=sys.stderr)

        elif not sizes:
            continue

        # look for a direction header
        if line.startswith('US: '):
            # print('direction UP', file=sys.stderr)
            direction = Direction.UP

        if line.startswith('DS: '):
            # print('direction DOWN', file=sys.stderr)
            direction = Direction.DOWN

        elif direction == Direction.NONE:
            continue

        # process bin entries
        m = re.fullmatch(r'bin=\s*([0-9]+):(.+)', line)
        if not m:
            continue

        base = int(m.group(1))
        # print('base=%d' % base, file=sys.stderr)
        for bin, val in enumerate(m.group(2).split(',')):
            for i in range(sizes[direction.value]):
                # print('index=%d dir=%d val=%s' % (base + bin + i, direction.value, val), file=sys.stderr)
                data[section][sizes[direction.value] * (base + bin) + i][direction.value] = int(val) if val.strip() else None

    return mode, data


# combine bins of 'step' elements
# the value of each bin is the mean of non-None values in the bin
def aggregate(ls, step):
    out = []
    s = c = j = 0
    for l in ls:
        if j == step:
            out.append(s / c if c else None)
            s = c = j = 0

        if l is not None:
            s += l
            c += 1

        j += 1

    if j:
        out.append(s / c if c else None)

    return out


# return a function that returns the first element of an iterable that is not in 'nones'
def coalescer(*nones):
    if not nones:
        nones = (None,)

    def coalesce(xs):
        for x in xs:
            if x not in nones:
                return x
        return None

    return coalesce


def write_fritz(sink, mode, data):

    if mode not in _steps:
        print('\n*** Error: Unsupported VDSL mode. Please send the output generated with \'-m raw\' to the developer.\n', file=sys.stderr)
        sys.exit(os.EX_DATAERR)

    with sink as out:
        # looks like the size of an HLOG/QLN bin depends on the VDSL mode (i.e. on the number of tones)
        step = _steps[mode]

        out.write('VDSL2 Profile: %s\n\n' % mode.value)

        if Section.HLOG in data:
            # reshape the data
            hlog_ds, hlog_us = zip(*(data[Section.HLOG][i] for i in range(8192)))

            out.write('HLOG DS Array: %s\n\n' % ','.join(str(round(x * 10)) if x else '-963' for x in aggregate(hlog_ds, step)))
            out.write('HLOG US Array: %s\n\n' % ','.join(str(round(x * 10)) if x else '-963' for x in aggregate(hlog_us, step)))
        else:
            print('\n*** Warning: no HLOG information\n', file=sys.stderr)

        if Section.QLN in data:
            # combine US/DS QLN data
            # -150 is Draytekese for 'no data'
            qln = map(coalescer(None, -150), (data[Section.QLN][i] for i in range(8192)))

            out.write('QLN Array: %s\n\n' % ','.join(str(round(x * 10)) if x else '-1505' for x in aggregate(qln, step)))
        else:
            print('\n*** Warning: no QLN information\n', file=sys.stderr)


def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i', '--input', required=False, help='read input from a file, use \'-\' for stdin')
    parser.add_argument('-H', '--host', default='192.168.1.1', help='telnet host')
    parser.add_argument('-P', '--port', type=int, default=23, help='telnet port')
    parser.add_argument('-u', '--username', required=False, help='telnet username')
    parser.add_argument('-p', '--password', required=False, help='telnet password')
    parser.add_argument('-m', '--mode', choices=['csv', 'fritz', 'raw'], default='fritz',
                        help='use -m csv to get a separate CSV file for each metric; use -m raw to dump raw output to a file')
    parser.add_argument('-o', '--output', required=False, help='write output to a file instead of stdout')

    args = parser.parse_args()

    if args.input:
        lines = sys.stdin if args.input == '-' else open(args.input, 'r')

    else:
        if not all((args.username, args.password)):
            print('\n*** Error: Username and password are required in telnet mode\n', file=sys.stderr)
            parser.print_help()
            sys.exit(os.EX_OK)

        commands = [
            'vdsl status',
            'vdsl status hlog',
            'vdsl status qln',
            'vdsl status snr',
        ]

        lines = telnet_exec(args.host, args.port, args.username, args.password, commands)

    if args.mode == 'raw':
        with out_file(args.output) as out:
            for line in lines:
                print(line, file=out)
        sys.exit(os.EX_OK)

    mode, data = parse_status(lines)

    if args.mode == 'csv':
        if Section.SNR in data:
            write_csv_dict(out_file(args.output, 'snr'), data[Section.SNR], 'SNR')

        if Section.QLN in data:
            write_csv_dict(out_file(args.output, 'qln'), data[Section.QLN], 'QLN')

        if Section.HLOG in data:
            write_csv_dict(out_file(args.output, 'hlog'), data[Section.HLOG], 'HLOG')

    elif args.mode == 'fritz':
        write_fritz(out_file(args.output), mode, data)


if __name__ == '__main__':
    main()
