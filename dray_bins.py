#!/usr/bin/env python3

import argparse
from enum import Enum
import os
import sys

from lib import out_file, write_csv
from lib.telnet import telnet_exec


class Direction(Enum):
    NONE = -1
    DOWN = 0
    UP = 1


def parse_showbins(file):
    state = Direction.NONE
    a_snr = [[0, 0] for _ in range(8192)]
    a_gain = [[0, 0] for _ in range(8192)]
    a_bits = [[0, 0] for _ in range(8192)]

    for line in file:
        if line.startswith('DOWNSTREAM'):
            state = Direction.DOWN

        elif line.startswith('UPSTREAM'):
            state = Direction.UP

        elif state != Direction.NONE:
            bins = line.split('*')
            if len(bins) == 4:
                for bin in bins:
                    tokens = list(map(int, bin.split()))
                    if len(tokens) == 4:
                        i, snr, gain, bits = tokens
                        a_snr[i][state.value] = snr if snr != 0 else 0
                        a_gain[i][state.value] = gain / 10 if gain != 0 else 0
                        a_bits[i][state.value] = bits if bits != 0 else 0

    return a_snr, a_gain, a_bits


def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i', '--input', required=False, help='read input from a file, use \'-\' for stdin')
    parser.add_argument('-H', '--host', default='192.168.1.1', help='telnet host')
    parser.add_argument('-P', '--port', type=int, default=23, help='telnet port')
    parser.add_argument('-u', '--username', required=False, help='telnet username')
    parser.add_argument('-p', '--password', required=False, help='telnet password')
    parser.add_argument('-m', '--mode', choices=['csv', 'raw'], default='csv',
                        help='use -m raw to dump raw output to a file')
    parser.add_argument('-o', '--output', required=False, help='write output to a file instead of stdout')

    args = parser.parse_args()

    if args.input:
        lines = sys.stdin if args.input == '-' else open(args.input, 'r')

    else:
        if not all((args.username, args.password)):
            print('\n*** Error: Username and password are required in telnet mode\n', file=sys.stderr)
            parser.print_help()
            sys.exit(os.EX_OK)

        commands = [
            'vdsl showbins',
            'vdsl showbins up',
        ]

        lines = telnet_exec(args.host, args.port, args.username, args.password, commands)

    if args.mode == 'raw':
        with out_file(args.output) as out:
            for line in lines:
                print(line, file=out)
        sys.exit(os.EX_OK)

    elif args.mode == 'csv':
        snr, gain, bits = parse_showbins(lines)

        write_csv(out_file(args.output, 'snr'), snr, 'SNR')
        write_csv(out_file(args.output, 'gain'), gain, 'GAIN')
        write_csv(out_file(args.output, 'bits'), bits, 'BITS')


if __name__ == '__main__':
    main()

