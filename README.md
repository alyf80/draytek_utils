# Doverosa premessa
Queste utility sono fornite "as is" senza alcuna pretesa di completezza o garanzia di funzionalità; se fanno quello che vi serve, bene, altrimenti pazienza.

# dray_stats.py
Utility che si connette via telnet ad un modem DrayTek ed estrae un file con le statistiche della linea in formato compatibile con il [Generatore di grafici HLogQLN di fibra.click](https://fibra.click/hlog/generatore/).

## Prerequisiti
* Python 3 (testato solo con python 3.8 sotto linux)
* modem VDSL DrayTek con accesso telnet abilitato (testato solo su un Vigor165 con firmware 4.1.1)

## Limitazioni
* testato solo su una linea con profilo 35b e rame marcio che non riesce nemmeno ad arrivare al tono 4096 (quello ho). In particolare, non ho idea di come e se cambino numero e dimensione dei bin riportati su linee 17a e con linee 35b che usano toni >4096
* vectoring non supportato (sulla mia linea non c'è, quindi non ho idea del relativo output)

Se vi interessa il supporto per i casi non coperti, aprite un'issue allegando l'output grezzo e cercherò di aggiungere quanto necessario.

## Esempi di utilizzo

Utilizzo tipico, scarica i dati dal modem e li salva nel file `stats.txt`:

	./dray_stats.py -H 192.168.1.1 -u admin -p password -o stats.txt

Per salvare l'output grezzo nel file `raw.txt`:

	./dray_stats.py -H 192.168.1.1 -u admin -p password -m raw -o raw.txt

Per generare le statistiche da un file di dati grezzi precedentemente salvato:

	./dray_stats.py -i raw.txt

Per estrarre le statistiche nei file `stats_hlog.csv`, `stats_qln.csv` e `stats_snr.csv`:

	./dray_stats.py [...] -m csv -o stats.csv

Per mostrare tutte le opzioni disponibili:

	./dray_stats.py -h


# dray_bins.py
Utility che si connette via telnet ad un modem DrayTek e scarica le statistiche per bin (comandi `vdsl showbins` e `vdsl showbins up`), salvandole in tre file csv contenenti i valori di snr, gain e bit loading per ciascun bin.

Per visualizzare le opzioni disponibili:

	./dray_bins.py -h
