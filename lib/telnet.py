import os
import re
import sys
import telnetlib

_user_prompts = [
    b'Account:',
]

_password_prompts = [
    b'Password:',
]

_error_res = [
    b'command fail',
    b'% Command missing',
    b'% Valid subcommands',
]


def _connection_error(e):
    print('\n*** Error: Unable to connect: %s\n' % e, file=sys.stderr)
    sys.exit(os.EX_IOERR)


def _login_error():
    print('\n*** Error: Unable to log in\n', file=sys.stderr)
    sys.exit(os.EX_IOERR)


def _credentials_error():
    print('\n*** Error: invalid username/password\n', file=sys.stderr)
    sys.exit(os.EX_NOPERM)


def _comm_error():
    print('\n*** Error: Communication failed\n', file=sys.stderr)
    sys.exit(os.EX_IOERR)


def _unsupported_command(command):
    print('\n*** Error: Command \'%s\' is not supported\n' % command, file=sys.stderr)


def _login(t, username, password):
    print("*** Logging in...", file=sys.stderr)

    i, m, _ = t.expect(_user_prompts, 5)
    if not m:
        _login_error()
    t.write(username.encode('ascii') + b'\n')

    i, m, _ = t.expect(_password_prompts, 5)
    if not m:
        _login_error()
    t.write(password.encode('ascii') + b'\n')

    i, m, _ = t.expect([b'.*>', *_password_prompts], 5)
    if not m:
        _login_error()

    elif i > 0:
        _credentials_error()

    # we can't use '>' for prompt matching as the output of the status command contains a '>' character,
    # so we extract the actual command prompt
    prompt = re.escape(m.group(0).strip())
    # print('*** Command prompt is "%s"' % prompt, file=sys.stderr)
    return prompt


def telnet_exec(host, port, username, password, commands):

    print("*** Connecting to %s:%d..." % (host, port), file=sys.stderr)

    try:
        with telnetlib.Telnet(host, port, 20) as t:

            prompt = _login(t, username, password)

            for command in commands:
                print('*** Sending command: "%s"' % command, file=sys.stderr)

                t.write(command.encode('ascii') + b'\n')

                again = True
                while again:
                    again = False
                    i, m, res = t.expect([prompt, *_error_res], 20)
                    if not m:
                        _comm_error()

                    if i > 0:
                        _unsupported_command(command)
                        again = True    # need to resync -- wait for the prompt
                    else:
                        for line in res.splitlines():
                            # clients want strings, not bytes...
                            yield line.decode('ascii')

            t.write(b'exit\n')

    except OSError as e:
        _connection_error(e)
