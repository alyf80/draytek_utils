import contextlib
import os
import sys


# Ew...
@contextlib.contextmanager
def out_file(name, qual=None):
    if (name or '-') == '-':
        yield sys.stdout

    else:
        if qual:
            name, ext = os.path.splitext(name)
            name = '%s_%s%s' % (name, qual, ext)

        print('Writing "%s"...' % name, file=sys.stderr)
        f = open(name, 'w')
        try:
            yield f
        finally:
            f.close()


def write_csv_dict(sink, dataset, label):
    with sink as out:
        out.write('bin; %s DS; %s US\n' % (label, label))
        for i, (down, up) in dataset.items():
            out.write('%d;%s;%s\n' % (i, str(down) if down else '', str(up) if up else ''))


def write_csv(sink, dataset, label):
    with sink as out:
        out.write('bin;%s DS; %s US\n' % (label, label))
        for i, (down, up) in enumerate(dataset, 0):
            out.write('%d;%d;%d\n' % (i, down, up))
